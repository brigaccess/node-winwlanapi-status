const node_winwlanapi = require('bindings')('node-winwlanapi-status');
exports.getWirelessStatus = node_winwlanapi.getWirelessStatus;

// https://docs.microsoft.com/en-us/windows/desktop/api/wlanapi/ne-wlanapi-_wlan_interface_state
exports.WIFI_NOT_READY = 0;
exports.WIFI_CONNECTED = 1;
exports.WIFI_AD_HOC_NETWORK_FORMED = 2;
exports.WIFI_DISCONNECTING = 3;
exports.WIFI_DISCONNECTED = 4;
exports.WIFI_ASSOCIATING = 5;
exports.WIFI_DISCOVERING = 6;
exports.WIFI_AUTHENTICATING = 7;