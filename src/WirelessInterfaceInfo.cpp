#include "WirelessInterfaceInfo.h"

vector<WirelessInterfaceInfo*>* GetWirelessIfacesInfo() {
	HANDLE clientHandle = NULL;
	// Client version for Vista and above, see 
    // https://docs.microsoft.com/en-us/windows/desktop/api/wlanapi/nf-wlanapi-wlanopenhandle
	DWORD clientVersion = 2;
	DWORD negotiatedVersion = 0;

	HRESULT result = WlanOpenHandle(clientVersion, NULL, &negotiatedVersion, &clientHandle);
	if (result != ERROR_SUCCESS) {
		// TODO Do something meaningful!
		return nullptr;
	}

	PWLAN_INTERFACE_INFO_LIST ifaceList;
	result = WlanEnumInterfaces(clientHandle, NULL, &ifaceList);
	if (result != ERROR_SUCCESS) {
		return nullptr;
	}
	
	PWLAN_INTERFACE_INFO iface;
	auto wiivector = new vector<WirelessInterfaceInfo*>();
	for (int i = 0; i < ifaceList->dwNumberOfItems; i++) {
		iface = (PWLAN_INTERFACE_INFO)& ifaceList->InterfaceInfo[i];
		wstring description = wstring(iface->strInterfaceDescription);
		wiivector->push_back(new WirelessInterfaceInfo(description, iface->isState));
	}

	if (ifaceList != NULL) {
		WlanFreeMemory(ifaceList);
		ifaceList = NULL;
	}
	
	WlanCloseHandle(clientHandle, NULL);
	return wiivector;
}