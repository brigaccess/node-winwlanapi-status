#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <string>;
#include <vector>;
#include <assert.h>

#include <stdio.h>
#include <stdlib.h>

#pragma comment(lib, "IPHLPAPI.lib")
#pragma comment(lib, "Ws2_32.lib")

using namespace std;

class IPInfo {
	string address;
	unsigned short type;
public:
	IPInfo(string address, unsigned short type) {
		this->address = address;
		this->type = type;
	}

	string* const getAddress() {
		return &this->address;
	}

	unsigned short getType() {
		return type;
	}
};

vector<IPInfo*>* GetIPInfo();