#include "IPInfo.h"

vector<IPInfo*>* GetIPInfo() {
	DWORD requiredSize = 0;
	
	PMIB_IPADDRTABLE addresses;
	HRESULT result = GetIpAddrTable(addresses, &requiredSize, 0);
	if (result == ERROR_INSUFFICIENT_BUFFER) {
		addresses = (MIB_IPADDRTABLE*)malloc(requiredSize);
		if (addresses == NULL) {
			return nullptr;
		}
	}
	result = GetIpAddrTable(addresses, &requiredSize, 0);
	if (result != NO_ERROR) {
		return nullptr;
	}
	
	auto wiivector = new vector<IPInfo*>();
	for (int i = 0; i < addresses->dwNumEntries; i++) {
		IN_ADDR ip;
		ip.S_un.S_addr = addresses->table[i].dwAddr;
#define BUF_SIZE 100
		char buf[BUF_SIZE];
		inet_ntop(AF_INET, &ip, buf, BUF_SIZE);
		wiivector->push_back(
			new IPInfo(string(buf), addresses->table[i].wType)
		);
	}
	
	free(addresses);
	return wiivector;
}


