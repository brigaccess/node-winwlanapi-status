#pragma once
#include <string>
#include <vector>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef WIN32_LEAN_AND_MEAN
#include <wlanapi.h>
#include <objbase.h>
#include <wtypes.h>

#include <stdio.h>
#include <stdlib.h>

#pragma comment(lib, "wlanapi.lib")
#pragma comment(lib, "ole32.lib")

using namespace std;

class WirelessInterfaceInfo {
	wstring description;
	WLAN_INTERFACE_STATE state;

public:
	WirelessInterfaceInfo(wstring description, WLAN_INTERFACE_STATE state) {
		this->description = description;
		this->state = state;
	}

	wstring* const getDescription() {
		return &(this->description);
	}

	WLAN_INTERFACE_STATE getInterfaceState() {
		return this->state;
	}
};

vector<WirelessInterfaceInfo*>* GetWirelessIfacesInfo();