#include <napi.h>
#include <locale>
#include <codecvt>

#include "WirelessInterfaceInfo.h"
#include "IPInfo.h"

#pragma comment(lib, "IPHLPAPI.lib")

using namespace std;
using convert_type = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_type, wchar_t> converter;

// Converts wstring to utf string and returns napi utf8 string value
Napi::String ValueFromWString(Napi::Env env, wstring* str) {
	return Napi::String::New(env, converter.to_bytes(*str));
}

Napi::Object GetWirelessStatusMethod(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	auto ifaces = GetWirelessIfacesInfo();

	Napi::Array result = Napi::Array::New(env);
	int index = 0;
	for (auto iter = ifaces->begin(); iter != ifaces->end(); iter++) {
		auto iface = *iter;
		Napi::Object obj = Napi::Object::New(env);
		obj.Set("description", ValueFromWString(env, iface->getDescription()));
		obj.Set("state", (int)iface->getInterfaceState());
		result.Set(index++, obj);
	}

	return result;
}

Napi::Object GetIPInfoMethod(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	auto ifaces = GetIPInfo();

	Napi::Array result = Napi::Array::New(env);
	int index = 0;
	for (auto iter = ifaces->begin(); iter != ifaces->end(); iter++) {
		auto ipinfo = *iter;
		Napi::Object obj = Napi::Object::New(env);
		obj.Set("ip", Napi::String::New(env, ipinfo->getAddress()->c_str()));
		obj.Set("type", (int)ipinfo->getType());
		result.Set(index++, obj);
	}

	return result;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	exports.Set(
		Napi::String::New(env, "getWirelessStatus"),
		Napi::Function::New(env, GetWirelessStatusMethod)
	);

	exports.Set(
		Napi::String::New(env, "getIPInfo"),
		Napi::Function::New(env, GetIPInfoMethod)
	);
	return exports;
}

NODE_API_MODULE(hello, Init)